package com.example.proyectoedilberto1.model

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tablaMascota")
data class Mascota(

    @PrimaryKey
    @NonNull
    val codigo:Int,

    @ColumnInfo(name="nombreMascota")
    val nombreMascota:String,

    @ColumnInfo(name="descripcion")
    val descripcion:String,

    @ColumnInfo(name="precio")
    val precio:Double,

    @ColumnInfo(name="imagen")
    val imagen:String,

    @ColumnInfo(name="categoria")
    val categoria:Int
)
