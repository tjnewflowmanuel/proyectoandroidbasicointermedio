package com.example.proyectoedilberto1.model

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tablaUsuario")
data class Usuario(

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "codigo")
    val codigo:Int ,

    @ColumnInfo(name = "nombres")
    val nombres:String,

    @ColumnInfo(name = "apellidoPaterno")
    val apellidoPaterno:String,

    @ColumnInfo(name = "apellidoMaterno")
    val apellidoMaterno:String,

    @ColumnInfo(name = "correo")
    val correo:String,

    @ColumnInfo(name = "clave")
    val clave:String
)
