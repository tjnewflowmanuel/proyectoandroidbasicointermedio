package com.example.proyectoedilberto1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.proyectoedilberto1.databinding.ActivityLoginBinding
import com.example.proyectoedilberto1.databinding.ActivityMenuBinding
import com.example.proyectoedilberto1.util.Constantes
import kotlinx.android.synthetic.main.activity_menu.*
import kotlinx.android.synthetic.main.activity_registro.*
import kotlinx.android.synthetic.main.activity_registro.btnInicio

class MenuActivity : AppCompatActivity() {

    lateinit var binding : ActivityMenuBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMenuBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnMascotas1.setOnClickListener {

            val bundle = Bundle().apply {
                putInt(Constantes.KEY_CATEGORIA,1)
            }

            val intent = Intent(this,MainActivity::class.java).apply {
                putExtras(bundle)
            }
            startActivity(intent)
        }

        binding.btnMascotas2.setOnClickListener {

            val bundle = Bundle().apply {
                putInt(Constantes.KEY_CATEGORIA,2)
            }

            val intent = Intent(this,MainActivity::class.java).apply {
                putExtras(bundle)
            }
            startActivity(intent)
        }
        btnProductos.setOnClickListener {
            val intent = Intent(this,InicioActivity::class.java)
            startActivity(intent)
        }
    }
}