package com.example.proyectoedilberto1.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.proyectoedilberto1.adapter.MascotaAdapter
import com.example.proyectoedilberto1.database.AppDatabase
import com.example.proyectoedilberto1.databinding.FragmentMascotaBinding
import com.example.proyectoedilberto1.model.Mascota
import com.example.proyectoedilberto1.viewmodel.CategoriaViewmodel
import java.util.concurrent.Executors

class MascotaFragment : Fragment() {

    private var mascotas = mutableListOf<Mascota>()
    private lateinit var binding : FragmentMascotaBinding

    private lateinit var categoriaViewmodel: CategoriaViewmodel

    private val appDatabase by lazy {
        activity?.let {
            AppDatabase.obtenerInstanciaBD(it)
        }

    }

    private val adaptador by lazy {
        MascotaAdapter()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentMascotaBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity?.let {

            categoriaViewmodel = ViewModelProvider(it).get(CategoriaViewmodel::class.java)
        }

        binding.recyclerMascotas.layoutManager = LinearLayoutManager(context)
        binding.recyclerMascotas.adapter = adaptador

        categoriaViewmodel.categoria().observe(viewLifecycleOwner, Observer {categoria->

            appDatabase?.mascotaDao()?.let {

                Executors.newSingleThreadExecutor().execute {

                    mascotas = it.obtenerMascotas(categoria)

                    activity?.runOnUiThread {
                        adaptador.actualizarMascotas(mascotas)
                    }

                }
            }

        })





    }




}