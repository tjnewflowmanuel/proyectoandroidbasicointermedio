package com.example.proyectoedilberto1.fragment

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.example.proyectoedilberto1.R
import java.lang.ClassCastException

class ProductoFragment : Fragment() {
    var boton:Button? = null
    var nombre:EditText? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_producto, container, false)
        boton = view.findViewById(R.id.boton)
        nombre = view.findViewById(R.id.etNombre)
        boton?.setOnClickListener {
            Toast.makeText(view.context, nombre?.text.toString(), Toast.LENGTH_SHORT).show()

        }
        return view
    }

    }
