package com.example.proyectoedilberto1.fragment

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.proyectoedilberto1.R
import com.example.proyectoedilberto1.databinding.FragmentMapaBinding
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import java.util.jar.Manifest

class MapaFragment : Fragment(), OnMapReadyCallback {

    private lateinit var binding: FragmentMapaBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMapaBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        solicitarPermiso()

        if (binding.mapa != null) {

            binding.mapa.onCreate(null)
            binding.mapa.onResume()
            binding.mapa.getMapAsync(this)
        }
    }

    private fun solicitarPermiso() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (context?.let {

                    ContextCompat.checkSelfPermission(
                        it,
                        android.Manifest.permission.ACCESS_FINE_LOCATION
                    )
                } == PackageManager.PERMISSION_GRANTED) {

            }
            else{

                activity?.let {
                    ActivityCompat.requestPermissions(it, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),1000
                    )
                }

            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if(grantResults[0] == PackageManager.PERMISSION_GRANTED){

        }else{

        }
    }

    @SuppressLint("MissingPermission")
    override fun onMapReady(googleMap: GoogleMap?) {

        googleMap?.mapType = GoogleMap.MAP_TYPE_TERRAIN

        val direccion = LatLng(-12.0889097, -77.0365457)

        val cameraPosition = CameraPosition.Builder()
            .target(direccion)
            .zoom(16f)
            .build()

        val cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition)
        googleMap?.animateCamera(cameraUpdate)

        val marcador = MarkerOptions()
            .icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_car))
            .position(direccion)

        googleMap?.addMarker(marcador)

        googleMap?.uiSettings?.isZoomControlsEnabled = true

        try{
            googleMap?.isMyLocationEnabled = true
        }catch (ex:Exception){
            ex.toString()
        }

    }


}