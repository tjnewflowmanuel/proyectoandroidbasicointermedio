package com.example.proyectoedilberto1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.proyectoedilberto1.database.AppDatabase
import com.example.proyectoedilberto1.databinding.ActivityLoginBinding
import com.example.proyectoedilberto1.util.Constantes
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_registro.*
import java.util.concurrent.Executors

class LoginActivity : AppCompatActivity() {

    private val flatActivo = 1
    private val flatInactivo = -1

    lateinit var binding : ActivityLoginBinding

    private val appDatabase by lazy{
        AppDatabase.obtenerInstanciaBD(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        recuperarPreferencia()

        binding.btnIngresar.setOnClickListener {
            autenticar()
        }

        binding.btnRegistrarme.setOnClickListener {

            val intent = Intent(this,RegistroActivity::class.java)
            startActivity(intent)
        }


    }

    private fun autenticar() {

        val email = binding.edtEmail.text.toString()
        val contrasenia = binding.edtContrasenia.text.toString()

        Executors.newSingleThreadExecutor().execute {

           val respuesta =  appDatabase.usuarioDao().autenticar(email,contrasenia)

            if(respuesta == 0){
                runOnUiThread {
                    Toast.makeText(this,"Credenciales incorrectas",Toast.LENGTH_SHORT).show()
                }
            }
            else{
                if(chkRecuerdame.isChecked) guardarPreferencia(email,contrasenia,flatActivo)
                else guardarPreferencia("","",flatInactivo)

                irMenuPrincipal()
            }

        }



    }

    private fun irMenuPrincipal() {

        val intent = Intent(this,MenuActivity::class.java)
        startActivity(intent)
    }

    private fun guardarPreferencia(email: String, contrasenia: String, flatActivo: Int) {

         getSharedPreferences(Constantes.PREFERENCIA_LOGIN,0).edit().apply {
            putString(Constantes.KEY_EMAIL,email)
            putString(Constantes.KEY_CONTRASENIA,contrasenia)
            putInt(Constantes.KEY_ESTADO_CHECKBOX,flatActivo)
            apply()
        }

    }

    private fun recuperarPreferencia(){

        val preferencia = getSharedPreferences(Constantes.PREFERENCIA_LOGIN,0)
        edtEmail.setText(preferencia.getString(Constantes.KEY_EMAIL,""))
        edtContrasenia.setText(preferencia.getString(Constantes.KEY_CONTRASENIA,""))

        val flatEstado = preferencia.getInt(Constantes.KEY_ESTADO_CHECKBOX,flatInactivo)

        chkRecuerdame.isChecked = flatEstado==1
    }
}