package com.example.proyectoedilberto1.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.proyectoedilberto1.model.Categoria
import com.example.proyectoedilberto1.model.Mascota
import com.example.proyectoedilberto1.model.Usuario
import java.util.concurrent.Executors


@Database(
    entities = [Usuario::class,Mascota::class,Categoria::class],
    version = 1,
    exportSchema = true
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun usuarioDao(): UsuarioDao
    abstract fun mascotaDao(): MascotaDao
    abstract fun categoriaDao(): CategoriaDao

    companion object{

        private var instancia : AppDatabase? = null

        fun obtenerInstanciaBD(context:Context) : AppDatabase{

            if(instancia == null){
                instancia = Room.databaseBuilder(
                    context,
                    AppDatabase::class.java,
                    "bdMascotas")
                    .addCallback(object : RoomDatabase.Callback(){

                        override fun onCreate(db: SupportSQLiteDatabase) {
                            super.onCreate(db)

                            Executors.newSingleThreadExecutor().execute {

                                val categorias = mutableListOf<Categoria>()
                                categorias.add(Categoria(1,"Mascotas1"))
                                categorias.add(Categoria(2,"Mascotas2"))

                                instancia?.categoriaDao()?.insertarCategorias(categorias)

                                val mascotas = mutableListOf<Mascota>()

                                mascotas.add(Mascota(1,"Perro","Todas las Razas",129.0,"",1))
                                mascotas.add(Mascota(2,"Gato","Todas las Razas",20.0,"",1))
                                mascotas.add(Mascota(3,"Loro","Todas las Razas",40.0,"",2))
                                mascotas.add(Mascota(4,"Gallina","Todas las Razas",20.0,"",2))
                                mascotas.add(Mascota(5,"Paloma","Todas las Razas",10.0,"",2))

                                instancia?.mascotaDao()?.insertar(mascotas)
                            }
                        }
                }).build()
            }
            return instancia as AppDatabase
        }

    }
}