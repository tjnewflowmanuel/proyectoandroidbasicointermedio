package com.example.proyectoedilberto1.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.proyectoedilberto1.model.Mascota

@Dao
interface MascotaDao {

    @Insert
    fun insertar(mascotas:MutableList<Mascota> )

    @Query("select *from tablaMascota where categoria=:categoriaInput")
    fun obtenerMascotas(categoriaInput:Int) : MutableList<Mascota>
}