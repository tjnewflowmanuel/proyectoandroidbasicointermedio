package com.example.proyectoedilberto1.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.proyectoedilberto1.model.Usuario

@Dao
interface UsuarioDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertarUsuario(usuario:Usuario)

    @Query("select count(*) from tablaUsuario where correo=:correoInput and clave=:claveInput")
    fun autenticar(correoInput:String, claveInput:String) : Int

}