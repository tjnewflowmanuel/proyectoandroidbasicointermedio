package com.example.proyectoedilberto1.database

import androidx.room.Dao
import androidx.room.Insert
import com.example.proyectoedilberto1.model.Categoria

@Dao
interface CategoriaDao {

    @Insert
    fun insertarCategorias(categorias: MutableList<Categoria>)
}