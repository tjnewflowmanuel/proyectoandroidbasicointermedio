package com.example.proyectoedilberto1.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.proyectoedilberto1.R
import com.example.proyectoedilberto1.databinding.ItemMascotaBinding
import com.example.proyectoedilberto1.model.Mascota

class MascotaAdapter(var mascotas:MutableList<Mascota> = mutableListOf()) : RecyclerView.Adapter<MascotaAdapter.MascotaAdapterViewHolder>(){

    class MascotaAdapterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

        private val binding = ItemMascotaBinding.bind(itemView)

        fun bind(mascota:Mascota){

            binding.tvTitulo.text = mascota.nombreMascota
            binding.tvDescripcion.text = mascota.descripcion
            binding.tvPrecio.text = mascota.precio.toString()
        }

    }

    fun actualizarMascotas(mascotas:MutableList<Mascota>){

        this.mascotas = mascotas
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MascotaAdapterViewHolder {

        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_mascota,parent,false)
        return MascotaAdapterViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mascotas.size
    }

    override fun onBindViewHolder(holder: MascotaAdapterViewHolder, position: Int) {

        val mascota = mascotas[position]
        holder.bind(mascota)

    }
}