package com.example.proyectoedilberto1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.proyectoedilberto1.database.AppDatabase
import com.example.proyectoedilberto1.database.UsuarioDao
import com.example.proyectoedilberto1.model.Usuario
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_registro.*
import java.util.concurrent.Executors

class RegistroActivity : AppCompatActivity() {

    val appDatabase:AppDatabase by lazy {
        AppDatabase.obtenerInstanciaBD(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registro)

        btnRegistro.setOnClickListener {
            registrarUsuario()
        }

        btnInicio.setOnClickListener {
            val intent = Intent(this,LoginActivity::class.java)
            startActivity(intent)
        }


    }

    private fun registrarUsuario() {

        try{

            val nombres = edtNombres.text.toString()
            val apellidoPaterno = edtApellidoPaterno.text.toString()
            val apellidoMaterno = edtApellidoMaterno.text.toString()
            val correo = edtCorreo.text.toString()
            val clave = edtClave.text.toString()

            val usuario = Usuario(0,nombres,apellidoPaterno,apellidoMaterno,correo,clave)

            Executors.newSingleThreadExecutor().execute {

                appDatabase.usuarioDao().insertarUsuario(usuario)

                runOnUiThread {
                    Toast.makeText(this,"Usuario grabado correctamente!!",Toast.LENGTH_SHORT).show()
                }
            }



        }catch (ex:Exception){
            Toast.makeText(this,ex.toString(),Toast.LENGTH_SHORT).show()
        }


    }
}