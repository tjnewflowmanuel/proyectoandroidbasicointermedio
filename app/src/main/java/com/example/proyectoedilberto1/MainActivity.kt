package com.example.proyectoedilberto1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.fragment.app.Fragment
import com.example.proyectoedilberto1.databinding.ActivityMainBinding
import com.example.proyectoedilberto1.fragment.MascotaFragment
import com.example.proyectoedilberto1.fragment.ProductoFragment
import com.example.proyectoedilberto1.util.Constantes
import com.example.proyectoedilberto1.viewmodel.CategoriaViewmodel

class MainActivity : AppCompatActivity() {

    lateinit var fragment : Fragment
    lateinit var binding : ActivityMainBinding

    private val categoriaViewmodel: CategoriaViewmodel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val bundle:Bundle? = intent.extras

        bundle?.let {

            val categoria = it.getInt(Constantes.KEY_CATEGORIA)
            categoriaViewmodel.recibirCategoria(categoria)

        }

        binding.btnMascotas.setOnClickListener {

            fragment = MascotaFragment()
            insertarFragmento()
        }

        binding.btnPruebas.setOnClickListener {

            fragment = ProductoFragment()
            insertarFragmento()
        }
    }

    fun insertarFragmento(){

        fragment?.let {

            supportFragmentManager.beginTransaction().replace(R.id.contenedor,it).commit()
        }

    }
}