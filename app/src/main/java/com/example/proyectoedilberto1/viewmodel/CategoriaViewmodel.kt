package com.example.proyectoedilberto1.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class CategoriaViewmodel : ViewModel() {

    fun categoria() : LiveData<Int>{
        return _categoria
    }

    private var _categoria : MutableLiveData<Int> = MutableLiveData()

    fun recibirCategoria(categoria:Int){ //1 o 2
        _categoria.value = categoria
    }
}